terraform {
  required_version = ">= 1.6"
  backend "s3" {
    bucket  = "wagensveld-tfstate"
    region  = "ap-southeast-2"
    encrypt = true
    # You may be expecting dynamodb_table here, but $3 a month is simply too expensive for the convenience of not having my tfstate messed up.
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.18"
    }
  }
}
