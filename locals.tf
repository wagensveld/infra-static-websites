locals {
  www_domain   = "www.${var.domain}"
  dmarc_record = "v=DMARC1;p=reject;rua=mailto:postmaster@${var.domain};ruf=mailto:admin@${var.domain};aspf=r;adkim=r;fo=1"
}
