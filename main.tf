resource "aws_route53_zone" "primary" {
  name = var.domain
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket" "website_content" {
  bucket = var.domain
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "website_content" {
  bucket = aws_s3_bucket.website_content.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "website_content" {
  bucket = aws_s3_bucket.website_content.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_ownership_controls" "website_content" {
  bucket = aws_s3_bucket.website_content.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "website_content" {
  bucket = aws_s3_bucket.website_content.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_acl" "website_content_acl" {
  depends_on = [
    aws_s3_bucket_ownership_controls.website_content,
    aws_s3_bucket_public_access_block.website_content,
  ]
  bucket = aws_s3_bucket.website_content.id
  acl    = "private"
}

resource "aws_s3_bucket_website_configuration" "website_content" {
  bucket = aws_s3_bucket.website_content.id
  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}

resource "aws_acm_certificate" "certificate" {
  domain_name               = var.domain
  validation_method         = "DNS"
  subject_alternative_names = [local.www_domain]
}

resource "aws_route53_record" "certificate_validation" {
  for_each = {
    for dvo in aws_acm_certificate.certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.primary.zone_id
}

resource "aws_acm_certificate_validation" "certificate" {
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.certificate_validation : record.fqdn]
}

resource "aws_cloudfront_function" "www_redirect" {
  name    = var.www_redirect
  runtime = "cloudfront-js-1.0"
  code    = file("./cf-function.js")
  publish = true
}

resource "aws_cloudfront_origin_access_control" "distribution" {
  name                              = var.domain
  description                       = "${var.domain} Policy"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

resource "aws_cloudfront_distribution" "distribution" {
  aliases             = [var.domain, local.www_domain]
  comment             = var.domain
  default_root_object = "index.html"
  enabled             = true


  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true


    default_ttl = 86400
    min_ttl     = 0
    max_ttl     = 31536000

    function_association {
      event_type   = "viewer-request"
      function_arn = aws_cloudfront_function.www_redirect.arn
    }

    target_origin_id       = var.domain
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  origin {
    domain_name              = aws_s3_bucket.website_content.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.distribution.id
    origin_id                = var.domain
  }

  http_version    = "http2and3"
  is_ipv6_enabled = true

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.certificate.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }
}

data "aws_iam_policy_document" "website_content" {
  statement {
    sid     = "AllowCloudFrontServicePrincipalReadOnly"
    effect  = "Allow"
    actions = ["s3:GetObject"]
    resources = [
      aws_s3_bucket.website_content.arn,
      "${aws_s3_bucket.website_content.arn}/*"
    ]

    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = [aws_cloudfront_distribution.distribution.arn]
    }
  }
}

resource "aws_s3_bucket_policy" "website_content" {
  bucket = aws_s3_bucket.website_content.id
  policy = data.aws_iam_policy_document.website_content.json
}

resource "aws_route53_record" "root" {
  zone_id = aws_route53_zone.primary.id
  name    = var.domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.primary.id
  name    = local.www_domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "mx" {
  zone_id = aws_route53_zone.primary.id
  name    = var.domain
  type    = "MX"
  ttl     = 300

  records = var.mx_records
}

resource "aws_route53_record" "spf" {
  zone_id = aws_route53_zone.primary.id
  name    = var.domain
  type    = "TXT"
  ttl     = 300

  records = [var.spf_record]
}

# Currently gitlab doesnt let matrix ingest variables, for now this will be a manual process
# resource "aws_route53_record" "email_security_key" {
#   zone_id = aws_route53_zone.primary.id
#   name    = "${var.security_key_record}.${var.domain}"
#   type    = "TXT"
#   ttl     = 300

#   records = [var.security_key_value]
# }


resource "aws_route53_record" "dmarc" {
  zone_id = aws_route53_zone.primary.id
  name    = "_dmarc.${var.domain}"
  type    = "TXT"
  ttl     = 300

  records = [local.dmarc_record]
}

# Default mailbox.org domainkeys
resource "aws_route53_record" "domain_key_1" {
  zone_id = aws_route53_zone.primary.id
  name    = "mbo0001._domainkey.${var.domain}"
  type    = "TXT"
  ttl     = 300

  records = ["v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2K4PavXoNY8eGK2u61LIQlOHS8f5sWsCK5b+HMOfo0M+aNHwfqlVdzi/IwmYnuDKuXYuCllrgnxZ4fG4yVaux58v9grVsFHdzdjPlAQfp5rkiETYpCMZwgsmdseJ4CoZaosPHLjP", "umFE/Ua2WAQQljnunsM9TONM9L6KxrO9t5IISD1XtJb0bq1lVI/e72k3mnPd/q77qzhTDmwN4TSNJZN8sxzUJx9HNSMRRoEIHSDLTIJUK+Up8IeCx0B7CiOzG5w/cHyZ3AM5V8lkqBaTDK46AwTkTVGJf59QxUZArG3FEH5vy9HzDmy0tGG+053/x4RqkhqMg5/ClDm+lpZqWwIDAQAB"]
}

resource "aws_route53_record" "domain_key_2" {
  zone_id = aws_route53_zone.primary.id
  name    = "mbo0002._domainkey.${var.domain}"
  type    = "TXT"
  ttl     = 300

  records = ["v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqxEKIg2c48ecfmy/+rj35sBOhdfIYGNDCMeHy0b36DX6MNtS7zA/VDR2q5ubtHzraL5uUGas8kb/33wtrWFYxierLRXy12qj8ItdYCRugu9tXTByEED05WdBtRzJmrb8YBMfeK0E0K3w", "woWfhIk/wzKbjMkbqYBOTYLlIcVGQWzOfN7/n3n+VChfu6sGFK3k2qrJNnw22iFy4C8Ks7j77+tCpm0PoUwA2hOdLrRw3ldx2E9PH0GVwIMJRgekY6cS7DrbHrj/AeGlwfwwCSi9T23mYvc79nVrh2+82ZqmkpZSTD2qq+ukOkyjdRuUPck6e2b+x141Nzd81dIZVfOEiwIDAQAB"]
}
