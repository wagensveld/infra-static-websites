variable "region" {
  type        = string
  description = "Default AWS region"
  default     = "us-east-1"
}

variable "domain" {
  type        = string
  description = "Domain for the website."
}

variable "mx_records" {
  type        = list(string)
  description = "List of mx records with their priorities."
  default     = ["10 mxext1.mailbox.org.", "10 mxext2.mailbox.org.", "20 mxext3.mailbox.org."]
}

variable "spf_record" {
  type        = string
  description = "SPF TXT record."
  default     = "v=spf1 include:mailbox.org ~all"
}

# variable "security_key_record" {
#   sensitive   = true
#   type        = string
#   description = "Mailbox security string record."
# }

# variable "security_key_value" {
#   sensitive   = true
#   type        = string
#   description = "Mailbox security string value."
# }

variable "www_redirect" {
  type        = string
  description = "Name for the cloudfront redirect function."
}
